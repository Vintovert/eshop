<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Log;
use Auth;
use Session;
use App\Models\User;
use App\Models\Order;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

 
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function adminUser()
    { 
        $user=User::find(Session::get('user')['id']);
        return view('adminPanel.user',compact('user'));
    }

    public function adminTable()
    {
        $data_orders=Order::all();
        
        return view('adminPanel.tables',['orders'=>$data_orders]);
    
    }

    public function admin()
    {
        $data=Product::all();
        
        return view('adminPanel.dashboard',['products'=>$data]);

    }
    public function delete($id)
    {
        
        $data=Product::find($id);
        if ($data != null)
         {
           
        $data->delete();
        // return view('adminPanel.dashboard',['product'=>$data]);
        }

        return redirect()->back();

        
    }
    public function add_image(Request $request)
    {
        
        return view('adminPanel.changeimage');
    }
    public function store(Request $request)
    {
        //$name = $request->name;
        // $image = $request->file('file');
        // $imageName = time().'.'.$image->extension();
        // $image->move(public_path('images'),$imageName);

        // $user = new User();
        // $user->name=$name;
        // $user->profileimage = $imageName;
        // $user->save();

        // return back()->with('user_added',"Success");
        if($request->hasFile('image')) {
            $file = $request->file('image');
            $name = $file->getClientOriginalName();
            $request->file('image')->move(base_path()."/public/images/", $name);
            // Product::where('id', '=', $id)->update(['gallery' => $name]);
            User::where('id',Session::get('user')['id'])->update(['gallery' => $name]);
            return redirect()->to('adminUser');
        }
        else
        {
            info('else');
        }
    }
    

    
}
