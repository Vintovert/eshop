<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Validator;
use Log;
use Session;
class CreateProductController extends Controller
{
	function viewcreate(Request $req)
	{


		return view('adminPanel.createproduct');

	}
	function post(Request $req)
	{

		$rules = array(
		    'name' => 'required',
		    'price' => 'required',
		    'description' => 'required'
		   );

		$validator = Validator::make( $req->all(), $rules);

		if ($validator->fails())
		{
		  $wrong = Session::put('adminmessage', 'Please fill out all the fields');

    		return back()->with('wrong', $wrong);
		}
		else
		{
				
		$product = new Product;
		$product->name=$req->name;
		$product->price=$req->price;
		$product->category=$req->category;
		$product->description=$req->description;
		$name = null;

		// $id = Product::insertGetId(['name' => $product->name, 'price' => $product->price, 'category' => $product->category, 'description' => $product->description]);

		if($req->hasFile('image')) {
            $file = $req->file('image');
            $name = $file->getClientOriginalName();
            $req->file('image')->move(base_path()."/public/images/", $name);
            // Product::where('id', '=', $id)->update(['gallery' => $name]);
            $product->gallery=$name;
        }

       $product->save();


		return redirect()->to('/admin');
		}

	}
}
