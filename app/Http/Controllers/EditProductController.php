<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Validator;
use Log;
use Input;
use Session;
class EditProductController extends Controller
{
	function viewedit(Request $req, $id)
	{
		$product=Product::find($id);

		return view('adminPanel.editproduct', compact('product'));

	}
	function update(Request $req)
	{

		  $rules = array(
		    'name' => 'required',
		    'price' => 'required',
		    'description' => 'required'
		   );

		$validator = Validator::make( $req->all(), $rules);

		if ($validator->fails())
		{
		  $wrong = Session::put('adminmessage', 'Please fill out all the fields');

    		return back()->with('wrong', $wrong);
		}
		else
		{
				$data=Product::find($req->id);
				$data->name=$req->name;
				$data->price=$req->price;
				$data->description=$req->description;
				$data->save();

				return redirect('admin');
		}

		

	}
	
}
