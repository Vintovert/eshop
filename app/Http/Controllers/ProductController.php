<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Models\Cart;
use App\Models\Order;
use Session;
use Illuminate\Support\Facades\DB;
use Auth;

class ProductController extends Controller
{
	function index()
	{

		$user = Session::get('user');
		$data=Product::all();
   		return view('product',['products'=>$data]);
	}
	function detail($id)
	{
		$data=Product::find($id);

		$relatedproducts =Product::where('category',$data->category)->get();
		return view('detail',['product'=>$data,'relatedproducts'=>$relatedproducts]);



	}
	function search(Request $request)
	{
		 $data=Product::where('name','like','%'.$request->input('query').'%')->get();
		 return view('search',['products'=>$data]);
	}
	function addToCart(Request $request)
	{
		if($request->session()->has('user'))
		{
			$cart = new Cart;
			$cart->user_id=$request->session()->get('user')['id'];
			$cart->product_id=$request->product_id;
			$cart->save();
			return redirect('/');

		}
		else
		{
			return redirect('/login');
		}

	}
	static function cartItem()
	{

		$userId=Session::get('user')['id'];
		return Cart::where('user_id',$userId)->count();
	}
	function cartList(Request $request )
	{
		if($request->session()->has('user'))
		{
			$userId=Session::get('user')['id'];
			$products=Cart::where('user_id',$userId)->get();
			$total_price = 0;

			foreach($products as $product){
				$total_price+= intval($product->product->price);
			}
			return view('cartlist',['products'=>$products,'total_price'=>$total_price]);
		}
		else
		{
			return redirect('/login');
		}
		
	}
	function removeCart($id)
	{
		Cart::destroy($id);
		return redirect('cartlist');
	}

	function orderNow(Request $request)
	{
		$total = $request->total_price;
		return view('ordernow',['total'=>$total]);
		
	}

	function orderPlace(Request $request)
	{
		$userId=Session::get('user')['id'];
		$allCart=Cart::where('user_id',$userId)->get();
		foreach($allCart as $cart)
		{
			$order = new Order;
			$order->product_id=$cart['product_id'];
			$order->user_id=$cart['user_id'];
			$order->status="pending";
			$order->payment_method=$request->payment;
			$order->payment_status="pending";
			$order->address=$request->address;
			$order->save();
			Cart::where('user_id',$userId)->delete();
		}
		 $request->input();
		 return redirect('/');

	}
	function myOrders(Request $request)
	{
		if($request->session()->has('user'))
		{
			$userId=Session::get('user')['id'];
		$orders=DB::table('orders')
		->join('products','orders.product_id','=','products.id')
		->where('orders.user_id',$userId)
		->get();

		return view('myorders',['orders'=>$orders]);

		}
		else
		{
			return redirect('/login');
		}
	}
	public function getData(){
      $product = Product::paginate(5);
      return view('adminPanel.dashboard', compact('product'));
    }
    

    public function getDiscount(Request $req){

    	$total_price=$req->total_price;
    	$user=User::find(Session::get('user')['id']);
    	$check=$user->coupon;
    	if($check>=1){
    		$check=$check-1;
    		$coupon_discount=$total_price*0.1;
    		
    		$total_price=$total_price-$coupon_discount;
			
			User::find(Session::get('user')['id'])->update(['coupon'=>$check]);
    		
    	}
    	return $total_price;
		}
}
