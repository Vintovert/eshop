<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;
use Illuminate\Support\Facades\Auth;
use Validator;
class UserController extends Controller
{
    


    function login(Request $req)
    {
        
       
    	$user = User::where(['email'=>$req->email])->first();

    	if(!$user)
    	{
            
            $wrong = Session::put('message', 'Incorect Username!');

    		return back();
            

    	}
    	if(!Hash::check($req->password,$user->password)){
            
             info("parola");
            $wrongpass = Session::put('message', 'Incorect Password!');

    		 return back();
           
    		
    	}
    	
		$req->session()->put('user',$user);
		return redirect('/');
    
    	

    }
    function register(Request $request)
    {
    	$user = new User;
    	$user->name=$request->name;
    	$user->email=$request->email;
    	$user->password=Hash::make($request->password);
        $user->usertype="user";
        $user->coupon=1;
    	$user->save();
    	return redirect('/login');

    }
    function update_user(Request $req)
    {
        $rules = array(
            'name' => 'required',
            'email' => 'required'
        );

        $validator = Validator::make( $req->all(), $rules);

        if ($validator->fails())
        {
          $wrong = Session::put('adminmessage', 'Please fill out all the fields');

            return back()->with('wrong', $wrong);
           
        }
    
        $user = User::find($req->user_id);

        $user->name=$req->name;
        $user->email=$req->email;
        $user->save();

        session()->forget('user');
        session()->put('user', $user);

        return redirect()->back();
        
    }
    function password_view()
    {
        

        return view('adminPanel.changepassword');
    }
    function change_password(Request $request)
    {
        $rules = array(
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required'
            
        );

        $validator = Validator::make( $request->all(), $rules);

        if ($validator->fails())
        {
            $wrong = Session::put('passwordmessage', 'Please fill out all the fields');
            return back()->with('wrong', $wrong);

            
        }

        $input = $request->all();
        $old_pass = $request->old_password;
        $new_password_hashed = Hash::make($request->new_password);
        $user_id = Session::get('user')['id'];
        $current_password=User::find($user_id)->password;

        if(password_verify($old_pass, $current_password)) 
        {
            
            if($input['new_password'] == $input['confirm_password'])
            {
                
                $new_password_hashed = password_hash($input['new_password'], PASSWORD_DEFAULT);
                User::where('id', '=', $user_id)->update(['password' => $new_password_hashed]);
                $right = Session::put('passwordmessage', 'Successful!');
                return redirect()->back()->with('right', $right);
                
            }
           
        }
        else{
        
            $wrong = Session::put('passwordmessage', 'Incorect Password');

            return redirect()->back()->with('wrong', $wrong);
           
        }

    }
    function login_form()
    {
        
        if(session()->has('user'))
        {
          return redirect('/');
        }
        return view('login');
    }
}
    


    

