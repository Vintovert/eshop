@extends('admin')

@section('titleAdmin')
Dashboard
@endsection

@section('dash')
    @if(session('adminmessage'))
      <div class="alert alert-success">{{session('adminmessage')}}</div>
    @endif
    <div class="main-panel" id="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-4">
            <div class="card card-user">
              <div class="image">
                <img src="../assets/img/bg5.jpg" alt="...">
              </div>
              <div class="card-body">
                <div class="author">
                    <input type="image" class="avatar border-gray" src="/images/{{Session::get('user')['gallery']}}" alt="...">
                    <form method="POST" action="/store" enctype="multipart/form-data">
                    	@csrf
					  <div class="mb-3">
						  <label for="formFile" class="form-label">Default file input example</label>
						  <input class="form-control" type="file" name="image" id="formFile">
						</div>
					  <button type="submit">Save</button>
					</form>
                    <h5 class="title">{{Session::get('user')['name']}}</h5>
                  <p class="description">
                    ADMIN
                  </p>
                </div>
                <h1 class="description text-center">ADMIN</h1>
              </div>
              <hr>
              <div class="button-container">
                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                  <i class="fab fa-facebook-f"></i>
                </button>
                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                  <i class="fab fa-twitter"></i>
                </button>
                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                  <i class="fab fa-google-plus-g"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script type="text/JavaScript" 
        src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js">
      </script>

      <script>
       $("input[type='image']").click(function() {
        $("input[id='my_file']").click();
      });
       
      </script>
      @endsection