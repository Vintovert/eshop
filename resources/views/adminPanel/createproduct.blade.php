@extends('admin')

@section('titleAdmin')
Dashboard
@endsection

@section('dash')
		@if(session('message'))
			<div id="alert" class="alert alert-success">
				<strong>{{Session::get('adminmessage')}}</strong>
				<button type="button" class="close" data-dismiss="alert" onclick="myFunction()">×</button> 
			</div>
			
		@endif
	<form action="/submit" method="POST" enctype="multipart/form-data">
		@csrf
	  <div class="form-group">
	    <label for="exampleFormControlInput1">Name</label>
	    <input type="name" class="form-control" name="name" id="exampleFormControlInput1" >
	  </div>
	  <div class="form-group">
	    <label for="exampleFormControlInput1">Price</label>
	    <input type="price" class="form-control" name="price" id="exampleFormControlInput1" >
	  </div>
	  <div class="form-group">
	    <label for="exampleFormControlSelect1">Category</label>
	    <select class="form-control" name="category" id="exampleFormControlSelect1">
	      <option>Food</option>
	      <option>Puzzles</option>
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="exampleFormControlTextarea1">Description</label>
	    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
	  </div>
	  <label class="form-label" for="customFile">Default file input example</label>
		<input name="image" type="file" class="form-control" id="customFile" />
	  <button type="submit" style="margin-top:10px;margin-left:10px;border-radius: 25px;">Submit</button>
	</form>

@endsection