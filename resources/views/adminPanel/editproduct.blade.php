@extends('admin')

@section('titleAdmin')
Dashboard
@endsection

@section('dash')
		@if(session('adminmessage'))
			<div class="alert alert-success">{{session('adminmessage')}}</div>
		@endif
	<form action="/edit/{{$product['id']}}" method="POST" enctype="multipart/form-data">
		@csrf
	  <div class="form-group">
	    <label for="exampleFormControlInput1">Name</label>
	    <input type="name" class="form-control" name="name" id="exampleFormControlInput1" value="{{$product['name']}}">
	  </div>
	  <div class="form-group">
	    <label for="exampleFormControlInput1">Price</label>
	    <input type="price" class="form-control" name="price" id="exampleFormControlInput1" value="{{$product['price']}}">
	  </div>
	  <div class="form-group">
	    <label for="exampleFormControlSelect1">category</label>
	    <select class="form-control" name="category" id="exampleFormControlSelect1">
	      <option>Food</option>
	      <option>Puzzles</option>
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="exampleFormControlTextarea1">Description</label>
	    <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" >{{$product['description']}}</textarea>
	  </div>
	  <label class="form-label" for="customFile">Default file input example</label>
		<input name="image" type="file" class="form-control" id="customFile" />
	  <button type="submit">Edit</button>
	</form>

@endsection