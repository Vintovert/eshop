@extends('admin')

@section('titleAdmin')
Dashboard
@endsection

@section('dash')
    @if(Session::has('adminmessage'))
      <div class="alert alert-success">{{Session::get('adminmessage')}}</div>
      <button type="button" class="close" data-dismiss="alert" onclick="myFunction()">×</button>
    @endif
    <div class="main-panel" id="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo">User Profile</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Edit Profile</h5>
              </div>
           
              <div class="card-body">
                <form action="/update_user" method="POST">
                  @csrf
                  <input type="hidden" name="user_id" value="{{Session::get('user')['id']}}">
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" value="{{Session::get('user')['email']}}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Company" value="{{Session::get('user')['name']}}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <button type="submit" style="border-style: none; background-color: white; color:#153254">Save changes</button>
                    </div>
                    <div class="col-md-4">
                      <a href="/change" style="text-decoration:none;">Change password</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-user">
              <div class="image">
                <img src="../assets/img/bg5.jpg" alt="...">
              </div>
              <div class="card-body">
                <div class="author">
                   <a href="/add-image"><input type="image" class="avatar border-gray" src="/images/{{$user->gallery}}" alt="..."></a>
                    <h5 class="title">{{Session::get('user')['name']}}</h5>
                  <p class="description">
                    ADMIN
                  </p>
                </div>
                <h1 class="description text-center">ADMIN</h1>
              </div>
              <hr>
              <div class="button-container">
                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                  <i class="fab fa-facebook-f"></i>
                </button>
                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                  <i class="fab fa-twitter"></i>
                </button>
                <button href="#" class="btn btn-neutral btn-icon btn-round btn-lg">
                  <i class="fab fa-google-plus-g"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script type="text/javascript">
        "{{session()->forget('adminmessage')}}"
      </script>
      @endsection