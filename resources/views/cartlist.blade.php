@extends('master')

@section("content")

<div class="custom-product">
	<div style="min-height: 90vh;">
 
			@foreach($products as $item)
			<br>
				<div class="row searched-item">
					<div class="col-sm-3">
						<div style=" "class="searched-item">
							<a style="text-decoration: none;" href="detail/{{$item->id}}">
								<img src="/images/{{$item->product->gallery}}" class="detail-image d-block " alt="...">
							</a>
						</div>
					</div>
					<div class="col-sm-3">
						<div style=" "class="searched-item">
								<h2 style=" color:black; ">{{$item->product->name}}</h2>
						        <h5 style=" color:black;">{{$item->product->description}}</h5>
						        <h3 style="color:black; ">{{$item->product->price}} BGN</h3>  
							</a>
						</div>
					</div>
					<div class="col-sm-3">
						<div style=" "class="searched-item">
							<a href="/removecart/{{$item->id}}" class="btn btn-warning">Remove from Cart</a>
						</div>
					</div>
				</div>
				<hr style="border-top: 0px;">
				@endforeach
				<form method="POST" action="/ordernow">
					@csrf
					<div class="trending-wrapper">
					  	<h4 >Total price:</h4>
					  	<div class="row">
					  		<div class="col-sm-6">
					  			<h3 id="total_price_html">{{$total_price}}BGN</h3>
					  		</div>
					  		<div class="col-sm-1">
					  			<input type="hidden" id="total_price_val" name="total_price" value="{{$total_price}}">
					  			<button type="submit" class="btn btn-success">Order Now</button> 			
					  		</div>
					  		<div class="col-sm-3">
					  			<button id="button" type="button" class="addtocart btn btn-primary">Use 10% disscount!</button>
					  		</div>
					  	</div>	
					</div>
				</form>


		            <script>
		            	$(document).ready(function(){
			            	$('#button').on('click',function(){
			            		var coupon="{{Session::get('user')['coupon']}}";
			            		
			            		let total_price = {{$total_price}};
			            		if(coupon<1)
			            		{
			           
			            			alert("You have used all of your disscounts!");
			            		}
			            		else
								{
				            		

				            		$.ajax({
										data: {total_price:total_price},
										method: "POST",
										url: '/get-disscount',
										headers: {
						                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						                }
										}).done(function (discount_price) {
											{
												console.log(discount_price)
												$('#total_price_html').html(discount_price+' BGN');
												$('#total_price_val').val(discount_price);
											}
										});
								}
			            	
			            	 });	
				        });

		            </script>	        
</div>
	
@endsection