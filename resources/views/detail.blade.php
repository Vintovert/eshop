@extends('master')

@section("content")
<div style="min-height: 100vh;">
    @if(session('error'))
      <div class="alert alert-success">{{session('error')}}</div>
    @endif
<div class="container">
  <a href="/">Go Back</a>
  <div class="row">
    <div class="col-sm-6">
      <img class="detail-image" src="/images/{{$product['gallery']}}">
    </div>
    <div class="col-sm-6">
          <h2 style=" color:black;">{{$product['name']}}</h2>
          <hr>
          <h4 style=" color:black;">Details: {{$product['description']}}</h4>
          <h5 style=" color:black;">Category:{{$product['category']}}</h5>
          <br><br>
          <div class="row">
            <div class="col-sm-6">
              <h3  style="color:black; ">{{$product['price']}} BGN</h3>
            </div>
            <div class="col-sm-6">
              <form action="/add_to_cart" method="POST">
              @csrf
              <input type="hidden" name="product_id" value={{$product['id']}}>
              <button class="addtocart btn btn-primary">Add to Cart</button>
            </form>   
            </div>
        </div> 
    </div>
  </div>
</div>
<div style="margin-top:20px;"class="shadow-lg container-fluid lenta">
    <!-- lenta -->
  </div>
 <div style="margin-top:20px;" class="relatedproducts container-fluid">
    <div class="row">
      <h3>Related Products</h3>
      <hr>
      @foreach($relatedproducts as $item)
      <div class="col-sm-2">
        <div style="text-align: -webkit-center;" class="trending-item">
          <a style="text-decoration: none;" href="/detail/{{$item['id']}}">
          <img style=" width: auto;height: auto;" src="/images/{{$item['gallery']}}" class="detail-image d-block w-50" alt="...">
          <h3 style=" color:black; text-align: center;">{{$item['name']}}</h3>
          </a>
        </div>
        </div>
        @endforeach
        <hr>      
    </div>
</div>
@endsection