<?php
use App\Http\Controllers\ProductController;
$total=0;
if(Session::has('user'))
{
  $total = ProductController::cartItem();

}
?>

<nav class=" header navbar navbar-expand-lg navbar-light">
  <div class=" container-fluid">
    <a class="navbar-brand" href="/" style="color:white;">Pazar.bg</a>
    <button class="navbar-toggler navbar-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="/" style="color:white;">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/myorders" style="color:white;">My Orders</a>
        </li>
      <form action="/search" class="d-flex" >
        <input class="search-box form-control me-2"name="query"type="search" placeholder="Search" aria-label="Search">
        <button class="header-text btn btn-outline-success" type="submit">Search</button>
      </form>
      </ul>
      <ul class=" nav navbar-nav navbar-right">
        <li style="margin-right:20px;margin-top:5px;"><a style="text-decoration: none;" href="/cartlist" ><i class="cart fa fa-shopping-cart"> Cart({{$total}})</i></a>
        </li>
        <!-- dropdown -->
        @if(Session::has('user'))
          <div class="dropdown">
            <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
              {{Session::get('user')['name']}}
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
              <li><a class="dropdown-item" href="/logout">Logout</a></li>
            </ul>
          </div>
          @else
          <div class="dropdown">

            <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false" style="color:white;">
              Account
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2" style="position: absolute;">
              <li><a class="dropdown-item" href="/logout">Login</a></li>
              <li><a class="dropdown-item" href="/register">Register</a></li>
            </ul>
          </div>
          @endif
        <!-- dropdown -->
      </ul>
    </div>
  </div>
</nav>

