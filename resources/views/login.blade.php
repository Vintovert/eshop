@extends('master')
@section("content")
<div style="min-height: 80vh;">
		@if(Session::has('message'))
		<div id="alert" class="alert alert-danger">
			<strong>{{Session::get('message')}}</strong>
			<button type="button" class="close" data-dismiss="alert" onclick="myFunction()">×</button> 
		</div>
		
		@endif
		
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2 mb-5 mt-5" >
				<form action="login" method="POST">
					@csrf
					<div class="mb-3">
						 <label for="exampleInputEmail1" class="form-label">Email address</label>
						 <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
					</div>
					<div class="mb-3">
						 <label for="exampleInputPassword1" class="form-label">Password</label>
						 <input type="password" name="password" class="form-control" id="exampleInputPassword1">
					</div>
					<div class="mb-3 form-check">
						 <input type="checkbox" class="form-check-input" id="exampleCheck1">
						 <label class="form-check-label" for="exampleCheck1">Remember me</label>
					 </div>
					 <button  type="submit" class="btn btn-primary">Login</button>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		"{{session()->forget('message')}}";
		function myFunction() {
    	// document.getElementById('alert').style.cssText = 'display:none';

		}

		$(document).ready(function() {
			$('.close').on('click', function() {
				$('#alert').slideUp();
			});
		});
	</script>
@endsection