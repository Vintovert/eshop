<!DOCTYPE html>
	<html>
		<head class="head">
			<title>E-comm Project</title>
			<!-- Latest compiled and minified CSS -->
				<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
<!-- 				<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
 -->				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

				
				<!-- jQuery -->
				<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

				<!-- Latest compiled and minified JavaScript -->
				<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
				
				<!-- Additional -->
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<meta name="csrf-token" content="{{ csrf_token() }}" />

				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
				<!-- CSS -->
				<style type="text/css">

					.nav-link:hover
					{
						background-color:#2f395f;
					}
					.close
					{
						float:right;
						border-style:none;
						border-radius: 25px;	
					}
					.cart:hover
				 	{
  						color: #198754;
					}
					.cart
					{
						color:white;

					}
					.detail-image
					{
						height:200px;
						width:200px;
					}
					.search-box
					{
						
						width:500px !important;
					}
					.footer
				 	{
				 		height: auto;
 						bottom:0;
				 		color:white;
				 		background-color:#181d31;  
					}
					.header
					{
						background-color:#181d31;
					}
					.header-text
					{

						color:white;
					}
					.carousel-image
					{
						margin-left: auto;
    					margin-right: auto;
					}
					
					.mostsold
					{
						margin-top: 100px;
						margin-bottom: 100px;
						text-align: center;
					}
					.productimg
					{

						margin-left: 25%;
					}
					.lenta
					{
						height:60px;
						background-color:#181d31;
					}
					.viewproduct
					{
						background-color: #181d31;
						 border-color: #181d31;
					}
					.addtocart
					{

						background-color: #181d31;
						 border-color: #181d31;
					}
					.carouselslider
					{
						background-color:black !important;
					}
					.footer-text
					{
						margin-top:25px;
					}
					@media only screen and (max-width: 1000px)
					{
						.mediatext
						{
							display:none;
						}
					}

					@media only screen and (max-width: 990px) {
					 .media
					   {
					    display: none;
					  }
					  .lenta
					  {
					  	display:none;
					  }
					  .mostsold
					{
						margin-top: 10px;
						margin-bottom: 10px;
						text-align: center;
					}
					.search-box
					{
						
						width:250px !important;
					}
					html,body
					{
					    width: 100%;
					    height: 100%;
					    margin: 0px;
					    padding: 0px;
					    overflow-x: hidden; 
					}
					.show-bar
					{
						position: absolute;
					}


				</style>
				<!-- CSS -->
		</head>

		<body class="body">
			@include('header')
			
			@yield('content')
</div>
			@include('footer')

		</body>
	</html>