@extends('master')

@section("content")

<div class="custom-product">
	<div style="min-height: 150vh;">
  <div class="trending-wrapper">
  	<h4 >My orders</h4>
			@foreach($orders as $item)
			<hr>
				<div class="row searched-item">
					<div class="col-sm-3">
						<div style=" "class="searched-item">
							<a style="text-decoration: none;" href="detail/{{$item->id}}">
								<img src="/images/{{$item->gallery}}" class="detail-image d-block " alt="...">
							</a>
						</div>
					</div>
					<div class="col-sm-3">
						<div style=" "class="searched-item">
								<h2 style="color:black;">Name: {{$item->name}}</h2>
						        <h5 style="color:black;">Delivery: {{$item->status}}</h5>
						        <h5 style="color:black;">Address: {{$item->address}}</h5>
						        <h5 style="color:black;">Payment status: {{$item->payment_status}}</h5>
						        <h5 style="color:black;">Payment Method: {{$item->payment_method}}</h5>
							</a>
						</div>
					</div>
				</div>
				@endforeach
	
</div>	
@endsection