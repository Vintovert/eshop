@extends('master')

@section("content")
	<div style="min-height: 80vh;">
	<style type="text/css">
			/* Style inputs with type="text", select elements and textareas */
		input[type=text], select, textarea {
		  width: 100%; /* Full width */
		  padding: 12px; /* Some padding */ 
		  border: 1px solid #ccc; /* Gray border */
		  border-radius: 4px; /* Rounded borders */
		  box-sizing: border-box; /* Make sure that padding and width stays in place */
		  margin-top: 6px; /* Add a top margin */
		  margin-bottom: 16px; /* Bottom margin */
		  resize: vertical /* Allow the user to vertically resize the textarea (not horizontally) */
		}

		/* Style the submit button with a specific background color etc */
		input[type=submit] {
		  background-color: #4CAF50;
		  color: white;
		  padding: 12px 20px;
		  border: none;
		  border-radius: 4px;
		  cursor: pointer;
		}

		/* When moving the mouse over the submit button, add a darker green color */
		input[type=submit]:hover {
		  background-color: #45a049;
		}

		/* Add a background color and some padding around the form */
		.container {
		  border-radius: 5px;
		 
		  padding: 20px;
		}
		input[type=email], select, textarea {
		  width: 100%; /* Full width */
		  padding: 12px; /* Some padding */ 
		  border: 1px solid #ccc; /* Gray border */
		  border-radius: 4px; /* Rounded borders */
		  box-sizing: border-box; /* Make sure that padding and width stays in place */
		  margin-top: 6px; /* Add a top margin */
		  margin-bottom: 16px; /* Bottom margin */
		  resize: vertical /* Allow the user to vertically resize the textarea (not horizontally) */
		}
	</style>
	<div class="custom-product">
		<div class="col-sm-10">
					<table class="table">
			  <tbody>
			    <tr>
			      <td>Amount</td>
			      <td>{{$total}} BGN</td>
			    </tr>
			    <tr>
			      <td>Tax</td>
			      <td>0 BGN</td>
			    </tr>
			    <tr>
			      <td >Delivery</td>
			      <td>0 BGN</td>
			    </tr>
			    <tr>
			      <td >Total Amount</td>
			      <td>{{$total}} BGN</td>
			    </tr>
			  </tbody>
			</table>
			<div class="row">
				<form action="/orderplace" method="POST" >
					@csrf
			    <label for="fname">Address</label>
			    <input type="text" id="fname" name="address" placeholder="Enter your address.. " required="Address">
			    <label for="pwd"><b>Payment Method:</b></label>
			    <select class="form-control" name="payment" id="exampleFormControlSelect1">
				      <option>Cash</option>
				      <option>Credit</option>
				      <option>PayPal</option>
				    </select>
			    <input type="submit" value="Order Now">
			  </form>
			</div>
		</div>
	</div>
@endsection