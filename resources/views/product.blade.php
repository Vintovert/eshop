@extends('master')

@section("content")

<div id="carouselExampleCaptions" class="carousel slide media" data-bs-ride="carousel">
  
  <div class="carousel-inner">
    @foreach($products as $item)
    
    <div class="carousel-item item {{$item['id']==1?'active':''}}">
    	<a href="detail/{{$item['id']}}">
    		<div class="row">
    			<img src="/images/{{$item['gallery']}}" class="d-block w-25 carousel-image"style="max-height:250px;max-width:300px" alt="...">
    		</div>
    	<div class="row">
    		<div class="  carousel-caption d-none d-md-block">       	        
				<h5 class="mediatext" style=" color:black;">{{$item['name']}}</h5>
				<p class="mediatext" style=" color:black;">{{$item['description']}}</p>			
      		</div>	
    	</div>
    	
    	
    		
     </a>
    </div>
     
	@endforeach
	
  </div>
  <button style="background-color:#dedcdc" class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button style="background-color:#dedcdc" class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>
	<div class="shadow-lg container-fluid lenta">
		<!-- lenta -->
	</div>

		<h1 class="mostsold">Most sold</h1>


		<div class="card-group">
			
		  <div class="card">
		   <a  href="/detail/5"> <img src="https://www.rubiks.com/static/version1607342405/frontend/Bestresponse/rubiks/en_GB/images/success_rubiks_popup.png" class="card-img-top w-50 productimg" alt="..."> </a>
		    <div class="card-body">
		      <h5 class="card-title">Card title</h5>
		      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
		      
				    <a href="/detail/5"><button class="viewproduct btn btn-primary">View Product</button></a>
				  
		    </div>
		    
		  </div>
		  
		  <div class="card">
		   <a href="/detail/4"> <img src="https://www.peterfisk.com/wp-content/uploads/2020/06/crocs-literide-clog.jpg" class="card-img-top w-50 productimg" alt="..."></a>
		    <div class="card-body">
		      <h5 class="card-title">Card title</h5>
		      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
		      <a href="/detail/4"><button class="viewproduct btn btn-primary">View Product</button></a>
		    </div>
		  </div>
		  <div class="card">
		   <a href="/detail/6"> <img src="https://5.imimg.com/data5/XO/JL/MY-57632048/butter-croissant-500x500.jpg" class=" productimg card-img-top w-50" alt="..."></a>
		    <div class="card-body">
		      <h5 class="card-title">Card title</h5>
		      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
		      <a href="/detail/6"><button class="viewproduct btn btn-primary">View Product</button></a>
		    </div>
		  </div>
		
		</div>

	
	
	

@endsection