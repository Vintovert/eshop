@extends('master')

@section("content")
<div style="min-height: 80vh;">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2 mb-5 mt-5" >
				<form action="register" method="POST">
					@csrf
					<div class="mb-3">
						 <label for="name" class="form-label">Username</label>
						 <input type="text" name="name" class="form-control" id="name" placeholder="Username">
					</div>
					<div class="mb-3">
						 <label for="exampleInputEmail1" class="form-label">Email address</label>
						 <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
					</div>
					<div class="mb-3">
						 <label for="exampleInputPassword1" class="form-label">Password</label>
						 <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					 <button  type="submit" class="btn btn-primary">Register</button>
				</form>
			</div>
		</div>
	</div>

@endsection