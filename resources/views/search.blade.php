@extends('master')

@section("content")
	<div style="min-height: 90vh;">
			@foreach($products as $item)	
    
				<div class="row searched-item">
						<hr>
					<div class="col-sm-2 ">
						<div style=" "class="searched-item">
							<a style="text-decoration: none;" href="detail/{{$item->id}}">
								<img src="/images/{{$item->gallery}}" class="detail-image d-block" alt="...">
							</a>
						</div>
					</div>
					<div class="col-sm-2">
						<div style=" "class="searched-item">
								<h2 style=" color:black; ">Name: {{$item->name}}</h2>
						        <h5 style=" color:black;">Price: {{$item->price}}</h5>
						         <a href="detail/{{$item->id}}"><button class="viewproduct btn btn-primary">View Product</button></a>
							</a>
						</div>
					</div>
				</div>
				@endforeach
@endsection