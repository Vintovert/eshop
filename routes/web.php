<?php

use Illuminate\Support\Facades\Route;
use App\Http\controllers\UserController;
use App\Http\controllers\ProductController;
use App\Http\controllers\AdminController;
use App\Http\controllers\CreateProductController;
use App\Models\Product;
use App\Models\Create;
use App\Http\controllers\EditProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [UserController::class,'login_form']);
Route::get('/logout', function () {
	Session::forget('user');
    return redirect('login');
});

//password
Route::post('change_password',[UserController::class,'change_password'])->middleware('admin');

//user
Route::post('update_user',[UserController::class,'update_user'])->middleware('admin');
Route::get('/change',[UserController::class,'password_view'])->middleware('admin');
Route::get('/add-image',[AdminController::class,'add_image']);
Route::post('/store',[AdminController::class,'store']);


//editcontroller
Route::get("edit/{id}",[EditProductController::class,'viewedit'])->name('edit')->middleware('admin');
Route::post("edit/{id}",[EditProductController::class,'update'])->middleware('admin');
//createcontroller
Route::get("/createproduct",[CreateProductController::class,'viewcreate'])->middleware('admin');
Route::post('/submit',[CreateProductController::class,'post'])->middleware('admin');


//admincontroller
Route::get("/adminUser",[AdminController::class,'adminUser'])->middleware('admin');
Route::get("/admin",[AdminController::class,'admin'])->middleware('admin');
Route::get("/adminTable",[AdminController::class,'adminTable'])->middleware('admin');
Route::delete("delete/{id}",[AdminController::class,'delete'])->name('delete-product');

//productcontroller
Route::get("/",[ProductController::class,'index']);
Route::get("detail/{id}",[ProductController::class,'detail']);
Route::get("search",[ProductController::class,'search']);
Route::post("add_to_cart",[ProductController::class,'addToCart']);
Route::get("cartlist",[ProductController::class,'cartList']);
Route::get("removecart/{id}",[ProductController::class,'removeCart']);
Route::post("ordernow",[ProductController::class,'orderNow']);
Route::post("orderplace",[ProductController::class,'orderPlace']);
Route::get("myorders",[ProductController::class,'myOrders']);
Route::get('admin', [ProductController::class, 'getData'])->middleware('admin');
Route::post('/coupon',[ProductController::class,'discount']);
Route::post('/get-disscount',[ProductController::class,'getDiscount']);


Route::view('/register','register');
Route::post("/login",[UserController::class,'login']);
Route::post("/register",[UserController::class,'register']);





